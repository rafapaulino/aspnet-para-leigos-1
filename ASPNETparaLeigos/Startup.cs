﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASPNETparaLeigos.Startup))]
namespace ASPNETparaLeigos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
